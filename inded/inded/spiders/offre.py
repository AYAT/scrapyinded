import scrapy
from scrapy.loader import ItemLoader
from inded.items import IndedItem


class OffreSpider(scrapy.Spider):
    name = "offre"
    start_urls = ["https://www.emploi.ma/recherche-jobs-maroc?f%5B0%5D=im_field_offre_metiers%3A31"]
    cols = ["title","company","sitweb","Métier","Secteur d´activité","Type de contrat",
            "Région","Niveau d'expérience", "Niveau d'études","Compétences clés",
            "Nombre de poste(s)"]
    
    def __init__(self):
        super().__init__()
        self.page_count =0
        self.offre_count =0
    

    def parse(self, response):

        offres = response.css("div.row")
        for offre in offres:
            url2 ="https://www.emploi.ma"
            url1 = offre.css("h5 a").attrib.get("href", "")

            yield scrapy.Request(
                url =url2+url1,
                callback = self.parse_details
            )
           
            
        next_btn = response.css("li.pager-next a")
        if next_btn:
            self.page_count +=1
            print("[******************************************** PAGE NUME ",self.page_count,"*********************************************]")
            next_page = f"{self.start_urls[0]}&{next_btn.attrib['href']}"
            yield scrapy.Request(url=next_page)
            
            
    def parse_details(self,response):

        title =response.css("h1.title::text").get()
        company =response.css("div.company-title a::text").get()
        sitweb =response.css("td.website-url a::text").get()

        table = response.css("table.job-ad-criteria")
        offre_details=[]
        
        loader = ItemLoader(item=IndedItem(),selector=table)
        
        for row in table.css("tr"):
           data = row.css("td div.field-item::text").get()
           offre_details.append(data)

           
        loader.add_value("title",title)
        loader.add_value("company",company)
        loader.add_value("sitweb",sitweb)
        loader.add_value("tableCriteres",offre_details)

        yield  loader.load_item()



