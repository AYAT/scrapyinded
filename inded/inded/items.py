from scrapy import Item, Field
from itemloaders.processors import MapCompose , TakeFirst

class IndedItem(Item):
    
    title =Field(output_processor=TakeFirst())
    
    company = Field(output_processor=TakeFirst())
    
    sitweb = Field(output_processor=TakeFirst())

    tableCriteres  = Field()
    
