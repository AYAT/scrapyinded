
from openpyxl import Workbook
from itemadapter.adapter import ItemAdapter

class IndedPipeline:

    def open_spider(self, spider):
        self.workbook = Workbook()
        self.sheet = self.workbook.active
        self.sheet.title= "ebooks"
        self.sheet.append(spider.cols)
    
    def process_item(self, item, spider):
        tableCriteres = item['tableCriteres']
        self.sheet.append([
            item['title'], item['company'], item['sitweb'],
            tableCriteres[0], tableCriteres[1], tableCriteres[2],
            tableCriteres[3], tableCriteres[4], tableCriteres[5],
            tableCriteres[6], tableCriteres[7]
        ])
    
    def close_spider(self,spider):
        self.workbook.save("ebooks.xlsx")
